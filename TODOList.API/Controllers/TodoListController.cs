﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TODOList.API.DTOs;
using TODOList.API.Enums;
using TODOList.API.Utils;
using TODOList.API.Interfaces;

namespace TODOList.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoListController : ControllerBase
    {
        private readonly ITodoListRepository _todoListRepository;

        public TodoListController(ITodoListRepository todoListRepository)
        {
            _todoListRepository = todoListRepository;
        }
        [HttpPost]
        public IActionResult Add([FromBody] TodoAddDto todoAddDto)
        {
            if (todoAddDto == null)
                return BadRequest();
            TodoViewDto createdItem = _todoListRepository.Add(todoAddDto.ToModel()).ToViewDto();

            return Created($"api/TodoList/{createdItem.Id}", createdItem);
        }


        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete([FromRoute]int id)
        {
            if (!_todoListRepository.Delete(id))
                return NotFound();
            return Ok();
        }


        [HttpPut]
        [Route("{id}")]
        public IActionResult Update([FromRoute]int id, [FromBody] TodoUpdateDto todoUpdateDto)
        {
            if (todoUpdateDto == null)
                return BadRequest();
            if (!_todoListRepository.Update(todoUpdateDto.ToModel(id)))
                return NotFound();
            return Ok();
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetById([FromRoute] int id)
        {
            TodoViewDto todoitem = _todoListRepository.GetById(id)?.ToViewDto();
            if (todoitem == null)
                return NotFound();
            return Ok(todoitem);
        }

        [HttpGet]
        public IActionResult GetAll([FromQuery] ToDoItemStatus? status)
        {
            if (status == null)
                return Ok(_todoListRepository.GetAll());
            return Ok(_todoListRepository.GetByStatus((ToDoItemStatus)status));

        }
    }
}
