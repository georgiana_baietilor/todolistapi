﻿namespace TODOList.API.Enums
{
    public enum ToDoItemStatus
    {
        Planned,
        InProgress,
        Done
    }
}
