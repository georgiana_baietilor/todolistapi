﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TODOList.API.Enums;

namespace TODOList.API.Model
{
    public class TodoListItem
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public DateTime Deadline { get; set; }
        public ToDoItemStatus Status { get; set; }
    }

    
}
