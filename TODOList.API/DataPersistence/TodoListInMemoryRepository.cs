﻿using System;
using System.Collections.Generic;
using System.Linq;
using TODOList.API.Enums;
using TODOList.API.Interfaces;
using TODOList.API.Model;

namespace TODOList.API.DataPersistence
{
    public class TodoListInMemoryRepository : ITodoListRepository
    {
        private Dictionary<int, TodoListItem> _todoItems;

        public TodoListInMemoryRepository()
        {
            _todoItems = new Dictionary<int, TodoListItem>();
            PopulateMock();
        }

        #region privateMethods
        private void PopulateMock()
        {
            _todoItems[1] = new TodoListItem()
            {
                Id = 1,
                Title = "Fix bug",
                Description = "Fix bug on project x",
                Deadline = DateTime.Now,
                Status = ToDoItemStatus.Planned
            };
            _todoItems[2] = new TodoListItem()
            {
                Id = 2,
                Title = "Fix bug",
                Description = "Fix bug on project y",
                Deadline = DateTime.Now,
                Status = ToDoItemStatus.InProgress
            };
            _todoItems[3] = new TodoListItem()
            {
                Id = 3,
                Title = "Design UI",
                Description = "Design UI project x",
                Deadline = DateTime.Now,
                Status = ToDoItemStatus.Done
            };
            _todoItems[4] = new TodoListItem()
            {
                Id = 4,
                Title = "Meeting",
                Description = "Meet John Doe",
                Deadline = DateTime.Now,
                Status = ToDoItemStatus.Planned
            };
            _todoItems[5] = new TodoListItem()
            {
                Id = 5,
                Title = "Fix bug",
                Description = "Fix bug on project z",
                Deadline = DateTime.Now,
                Status = ToDoItemStatus.InProgress
            };
        }
        #endregion

        public TodoListItem Add(TodoListItem item)
        {
            int newID = _todoItems.Keys.Max() + 1;
            item.Id = newID;
            item.Status = ToDoItemStatus.Planned;
            _todoItems[newID] = item;
            return item;
        }

        public bool Delete(int id)
        {
            return _todoItems.Remove(id);
        }

        public IEnumerable<TodoListItem> GetAll()
        {
            return _todoItems.Values;
        }

        public TodoListItem GetById(int id)
        {
            if (!_todoItems.ContainsKey(id))
                return null;
            return _todoItems[id];
        }

        public IEnumerable<TodoListItem> GetByStatus(ToDoItemStatus status)
        {
            return _todoItems.Values.Where(it => it.Status == status);
        }

        public bool Update(TodoListItem item)
        {
            if (!_todoItems.ContainsKey(item.Id))
                return false;
            _todoItems[item.Id] = item;
            return true;
        }
    }
}
