﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TODOList.API.Enums;

namespace TODOList.API.DTOs
{
    public class TodoUpdateDto
    {
        public string Description { get; set; }
        public string Title { get; set; }
        public DateTime Deadline { get; set; }
        public ToDoItemStatus Status { get; set; }
    }
}
