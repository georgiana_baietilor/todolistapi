﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TODOList.API.DTOs
{
    public class TodoAddDto
    {
        public string Description { get; set; }
        public string Title { get; set; }
        public DateTime Deadline { get; set; }
    }
}
