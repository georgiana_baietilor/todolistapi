﻿using System.Collections.Generic;
using TODOList.API.Enums;
using TODOList.API.Model;


namespace TODOList.API.Interfaces
{
    public interface ITodoListRepository
    {
        TodoListItem Add(TodoListItem item);
        bool Delete(int id);
        bool Update(TodoListItem item);
        TodoListItem GetById(int id);
        IEnumerable<TodoListItem> GetAll();
        IEnumerable<TodoListItem> GetByStatus(ToDoItemStatus status);
    }
}
