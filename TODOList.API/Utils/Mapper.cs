﻿using System.Collections.Generic;
using System.Linq;
using TODOList.API.DTOs;
using TODOList.API.Model;

namespace TODOList.API.Utils
{
    public static class Mapper
    {
        public static TodoListItem ToModel(this TodoAddDto todoAddDto)
        {
            return new TodoListItem
            {
                Description = todoAddDto.Description,
                Title = todoAddDto.Title,
                Deadline = todoAddDto.Deadline
            };
        }
        public static TodoViewDto ToViewDto(this TodoListItem TodoListItem)
        {
            return new TodoViewDto
            {
                Id = TodoListItem.Id,
                Deadline = TodoListItem.Deadline,
                Description = TodoListItem.Description,
                Title = TodoListItem.Title,
                Status = TodoListItem.Status
            };
        }
        public static TodoListItem ToModel(this TodoUpdateDto todoUpdateDto, int id)
        {
            return new TodoListItem
            {
                Id = id,
                Deadline = todoUpdateDto.Deadline,
                Description = todoUpdateDto.Description,
                Title = todoUpdateDto.Title,
                Status = todoUpdateDto.Status
            };
        }
        public static IEnumerable<TodoViewDto> TodoViewDto(this IEnumerable<TodoListItem> TodoListItems)
        {
            return TodoListItems.Select(x => x.ToViewDto());
        }

    }
}
